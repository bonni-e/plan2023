const cookie = document.cookie.split("; ");

cookie.forEach(e => {
    const data = e.split("=");
    const key = data[0];
    const value = data[1];
    
    if(key !== ""){ 
        const box = document.querySelector(`#${key}`);
        if(value === "true")
            box.checked = true;
        else
            box.checked = false;
    }
});

const checkbox = document.getElementsByClassName("todo");
console.log(checkbox.length);

for(let i=0; i<checkbox.length; i++){
    const box = checkbox[i];
    box.addEventListener("click", e => {
        if(box.checked)
            document.cookie = `${box.id}=true; path=/`
        else
            document.cookie = `${box.id}=false; path=/`
    });
}
